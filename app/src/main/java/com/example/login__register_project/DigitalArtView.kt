package com.example.login__register_project

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.login__register_project.databinding.ItemviewBinding

class DigitalArtView(private val itemviewBinding: ItemviewBinding, private val clickListerner: dgtArtClickListerner)
        : RecyclerView.ViewHolder(itemviewBinding.root){
      fun binddgtArt(digitalArt: DigitalArt){
          itemviewBinding.cover.setImageResource(digitalArt.dgtimage)
          itemviewBinding.title.text = digitalArt.dgttitle
          itemviewBinding.Dgtartist.text = digitalArt.dgtartists
          itemviewBinding.cardView.setOnClickListener {
              clickListerner.onClick(digitalArt)
          }
      }

}