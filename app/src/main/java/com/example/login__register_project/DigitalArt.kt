package com.example.login__register_project

var dgtArtList = mutableListOf<DigitalArt>()
val DGTART_ID_EXTRA = "digitalartExtra"

class DigitalArt( var dgtimage: Int, var dgtartists: String, var dgttitle: String, var dgtdesc: String,
                  val id: Int? = dgtArtList.size)